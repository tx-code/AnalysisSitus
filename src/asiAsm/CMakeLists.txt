project(asiAsm)

#------------------------------------------------------------------------------

set (H_FILES
  asiAsm.h
)
set (CPP_FILES
)

#------------------------------------------------------------------------------
# Entity
#------------------------------------------------------------------------------

set (entity_H_FILES
  entity/asiAsm_EntBase.h
  entity/asiAsm_EntPrototype.h
  entity/asiAsm_SceneTree.h
)
set (entity_CPP_FILES
  entity/asiAsm_SceneTree.cpp
)

#------------------------------------------------------------------------------
# XDE
#------------------------------------------------------------------------------

set (xde_H_FILES
  xde/asiAsm_XdeApp.h
  xde/asiAsm_XdeBreakDown.h
  xde/asiAsm_XdeDoc.h
  xde/asiAsm_XdeDocIterator.h
  xde/asiAsm_XdeFindDirtyParts.h
  xde/asiAsm_XdeGraph.h
  xde/asiAsm_XdePartRepr.h
  xde/asiAsm_XdePersistentIds.h
)
set (xde_CPP_FILES
  xde/asiAsm_XdeApp.cpp
  xde/asiAsm_XdeBreakDown.cpp
  xde/asiAsm_XdeDoc.cpp
  xde/asiAsm_XdeDocIterator.cpp
  xde/asiAsm_XdeFindDirtyParts.cpp
  xde/asiAsm_XdeGraph.cpp
)

#------------------------------------------------------------------------------
# Interoperability
#------------------------------------------------------------------------------

set (interop_H_FILES
  interop/fbx_XdeReader.h
  interop/fbx_XdeWriter.h
)
set (interop_CPP_FILES
  interop/fbx_XdeReader.cpp
  interop/fbx_XdeWriter.cpp
)

#------------------------------------------------------------------------------
# Interoperability
#------------------------------------------------------------------------------

set (interop_gltf_H_FILES
  interop/gltf/asiAsm_GLTFCSysConverter.h
  interop/gltf/asiAsm_GLTFEntities.h
  interop/gltf/asiAsm_GLTFJsonSerializer.h
  interop/gltf/asiAsm_GLTFMaterialAttr.h
  interop/gltf/asiAsm_GLTFMaterialMap.h
  interop/gltf/asiAsm_GLTFMaterialMapBase.h
  interop/gltf/asiAsm_GLTFXdeVisualStyle.h
  interop/gltf/asiAsm_GLTFWriter.h
  interop/gltf/asiAsm_GLTFPrimitive.h
  interop/gltf/asiAsm_GLTFSceneStructure.h
  interop/gltf/asiAsm_GLTFFacePropertyExtractor.h
  interop/gltf/asiAsm_GLTFIDataSourceProvider.h
  interop/gltf/asiAsm_GLTFXdeDataSourceProvider.h
)
set (interop_gltf_CPP_FILES
  interop/gltf/asiAsm_GLTFCSysConverter.cpp
  interop/gltf/asiAsm_GLTFMaterialAttr.cpp
  interop/gltf/asiAsm_GLTFMaterialMap.cpp
  interop/gltf/asiAsm_GLTFMaterialMapBase.cpp
  interop/gltf/asiAsm_GLTFXdeVisualStyle.cpp
  interop/gltf/asiAsm_GLTFWriter.cpp
  interop/gltf/asiAsm_GLTFSceneStructure.cpp
  interop/gltf/asiAsm_GLTFFacePropertyExtractor.cpp
  interop/gltf/asiAsm_GLTFXdeDataSourceProvider.cpp
)

#------------------------------------------------------------------------------
# OpenCascade libraries
#------------------------------------------------------------------------------

set (OCCT_LIB_FILES
  TKernel
  TKMath
  TKBRep
  TKOffset
  TKTopAlgo
  TKG2d
  TKG3d
  TKGeomBase
  TKGeomAlgo
  TKMesh
  TKShHealing
  TKFeat
  TKBool
  TKBO
  TKPrim
  TKBin
  TKBinL
  TKBinXCAF
  TKLCAF
  TKCDF
  TKCAF
  TKXCAF
  TKService
  TKXSBase
  TKSTEP
  TKIGES
  TKXDESTEP
  TKXDEIGES
)

#------------------------------------------------------------------------------
# Mobius
#------------------------------------------------------------------------------

if (USE_MOBIUS)
  set (MOBIUS_LIB_FILES
    mobiusCore
    mobiusPoly
    mobiusBSpl
    mobiusCascade
    mobiusGeom
  )
endif()

#------------------------------------------------------------------------------
# FBX SDK libraries
#------------------------------------------------------------------------------

if (WIN32)
  set (FBX_SDK_LIB_FILES
    libfbxsdk
  )
else()
  set (FBX_SDK_LIB_FILES
    libfbxsdk.a
  )
endif()

#------------------------------------------------------------------------------
# Add sources
#------------------------------------------------------------------------------

foreach (FILE ${H_FILES})
  set (src_files ${src_files} ${FILE})
  source_group ("Header Files" FILES "${FILE}")
endforeach (FILE)

foreach (FILE ${CPP_FILES})
  set (src_files ${src_files} ${FILE})
  source_group ("Source Files" FILES "${FILE}")
endforeach (FILE)

foreach (FILE ${entity_H_FILES})
  source_group ("Header Files\\Entity" FILES "${FILE}")
endforeach (FILE)

foreach (FILE ${entity_CPP_FILES})
  source_group ("Source Files\\Entity" FILES "${FILE}")
endforeach (FILE)

foreach (FILE ${xde_H_FILES})
  source_group ("Header Files\\XDE" FILES "${FILE}")
endforeach (FILE)

foreach (FILE ${xde_CPP_FILES})
  source_group ("Source Files\\XDE" FILES "${FILE}")
endforeach (FILE)

foreach (FILE ${interop_H_FILES})
  source_group ("Header Files\\Interoperability" FILES "${FILE}")
endforeach (FILE)

foreach (FILE ${interop_CPP_FILES})
  source_group ("Source Files\\Interoperability" FILES "${FILE}")
endforeach (FILE)

foreach (FILE ${interop_gltf_H_FILES})
  source_group ("Header Files\\Interoperability\\glTF" FILES "${FILE}")
endforeach (FILE)

foreach (FILE ${interop_gltf_CPP_FILES})
  source_group ("Source Files\\Interoperability\\glTF" FILES "${FILE}")
endforeach (FILE)

#------------------------------------------------------------------------------
# Configure includes
#------------------------------------------------------------------------------

# Create include variable
set (asiAsm_include_dir_loc "${CMAKE_CURRENT_SOURCE_DIR};\
  ${CMAKE_CURRENT_SOURCE_DIR}/entity;\
  ${CMAKE_CURRENT_SOURCE_DIR}/xde;\
  ${CMAKE_CURRENT_SOURCE_DIR}/interop;\
  ${CMAKE_CURRENT_SOURCE_DIR}/interop/gltf;\
")
#
set (asiAsm_include_dir ${asiAsm_include_dir_loc} PARENT_SCOPE)

include_directories ( SYSTEM
                      ${asiAsm_include_dir_loc}
                      ${asiActiveData_include_dir}
                      ${asiAlgo_include_dir}
                      ${asiData_include_dir}
                      ${3RDPARTY_OCCT_INCLUDE_DIR}
                      ${3RDPARTY_EIGEN_DIR}
                      ${3RDPARTY_rapidjson_DIR}
                      ${3RDPARTY_FBX_SDK_INCLUDE_DIR}
                      ${3RDPARTY_tbb_INCLUDE_DIR} )

if (USE_MOBIUS)
  include_directories(SYSTEM ${3RDPARTY_mobius_INCLUDE_DIR})
endif()

#------------------------------------------------------------------------------
# Create library
#------------------------------------------------------------------------------

add_library (asiAsm SHARED
  ${H_FILES}              ${CPP_FILES}
  ${entity_H_FILES}       ${entity_CPP_FILES}
  ${xde_H_FILES}          ${xde_CPP_FILES}
  ${interop_H_FILES}      ${interop_CPP_FILES}
  ${interop_gltf_H_FILES} ${interop_gltf_CPP_FILES}
)

target_link_libraries(asiAsm asiAlgo)

#------------------------------------------------------------------------------
# Dependencies
#------------------------------------------------------------------------------

if (3RDPARTY_tbb_LIBRARY_DIR_DEBUG)
  link_directories(${3RDPARTY_tbb_LIBRARY_DIR_DEBUG})
else()
  link_directories(${3RDPARTY_tbb_LIBRARY_DIR})
endif()

foreach (LIB_FILE ${OCCT_LIB_FILES})
  if (WIN32)
    set (LIB_FILENAME "${LIB_FILE}${CMAKE_STATIC_LIBRARY_SUFFIX}")
  else()
    set (LIB_FILENAME "lib${LIB_FILE}${CMAKE_SHARED_LIBRARY_SUFFIX}")
  endif()

  if (3RDPARTY_OCCT_LIBRARY_DIR_DEBUG AND EXISTS "${3RDPARTY_OCCT_LIBRARY_DIR_DEBUG}/${LIB_FILENAME}")
    target_link_libraries (asiAsm debug ${3RDPARTY_OCCT_LIBRARY_DIR_DEBUG}/${LIB_FILENAME})
    target_link_libraries (asiAsm optimized ${3RDPARTY_OCCT_LIBRARY_DIR}/${LIB_FILENAME})
  else()
    target_link_libraries (asiAsm ${3RDPARTY_OCCT_LIBRARY_DIR}/${LIB_FILENAME})
  endif()
endforeach()

foreach (LIB_FILE ${TBB_LIB_FILES})
  if (WIN32)
    set (LIB_FILENAME "${LIB_FILE}${CMAKE_STATIC_LIBRARY_SUFFIX}")
    set (LIB_FILENAME_DEBUG "${LIB_FILE}_debug${CMAKE_STATIC_LIBRARY_SUFFIX}")
  else()
    set (LIB_FILENAME "lib${LIB_FILE}${CMAKE_SHARED_LIBRARY_SUFFIX}")
    set (LIB_FILENAME_DEBUG "lib${LIB_FILE}_debug${CMAKE_SHARED_LIBRARY_SUFFIX}")
  endif()

  target_link_libraries (asiAsm debug ${3RDPARTY_tbb_LIBRARY_DIR}/${LIB_FILENAME_DEBUG})
  target_link_libraries (asiAsm optimized ${3RDPARTY_tbb_LIBRARY_DIR}/${LIB_FILENAME})
endforeach()

if (USE_FBX_SDK)
  foreach (LIB_FILE ${FBX_SDK_LIB_FILES})
    if (WIN32)
      set (LIB_FILENAME "${LIB_FILE}${CMAKE_STATIC_LIBRARY_SUFFIX}")
    else()
      set (LIB_FILENAME "lib${LIB_FILE}${CMAKE_SHARED_LIBRARY_SUFFIX}")
    endif()

    target_link_libraries (asiAsm debug ${3RDPARTY_FBX_SDK_LIBRARY_DIR}/${LIB_FILENAME})
    target_link_libraries (asiAsm optimized ${3RDPARTY_FBX_SDK_LIBRARY_DIR}/${LIB_FILENAME})
  endforeach()
endif()

#------------------------------------------------------------------------------
# Installation
#------------------------------------------------------------------------------

if (NOT BUILD_ALGO_ONLY)
  install (TARGETS asiAsm CONFIGURATIONS Release        RUNTIME DESTINATION bin  LIBRARY DESTINATION bin  COMPONENT Runtime)
  install (TARGETS asiAsm CONFIGURATIONS RelWithDebInfo RUNTIME DESTINATION bini LIBRARY DESTINATION bini COMPONENT Runtime)
  install (TARGETS asiAsm CONFIGURATIONS Debug          RUNTIME DESTINATION bind LIBRARY DESTINATION bind COMPONENT Runtime)
endif()

#------------------------------------------------------------------------------
# Installation of Analysis Situs as a framework
#------------------------------------------------------------------------------

install (TARGETS asiAsm
         CONFIGURATIONS Release
         RUNTIME DESTINATION ${SDK_INSTALL_SUBDIR}bin COMPONENT Runtime
         ARCHIVE DESTINATION ${SDK_INSTALL_SUBDIR}lib COMPONENT Library
         LIBRARY DESTINATION ${SDK_INSTALL_SUBDIR}lib COMPONENT Library)

install (TARGETS asiAsm
         CONFIGURATIONS RelWithDebInfo
         RUNTIME DESTINATION ${SDK_INSTALL_SUBDIR}bini COMPONENT Runtime
         ARCHIVE DESTINATION ${SDK_INSTALL_SUBDIR}libi COMPONENT Library
         LIBRARY DESTINATION ${SDK_INSTALL_SUBDIR}libi COMPONENT Library)

install (TARGETS asiAsm
         CONFIGURATIONS Debug
         RUNTIME DESTINATION ${SDK_INSTALL_SUBDIR}bind COMPONENT Runtime
         ARCHIVE DESTINATION ${SDK_INSTALL_SUBDIR}libd COMPONENT Library
         LIBRARY DESTINATION ${SDK_INSTALL_SUBDIR}libd COMPONENT Library)

if (MSVC)
  install (FILES ${PROJECT_BINARY_DIR}/../../${PLATFORM}${COMPILER_BITNESS}/${COMPILER}/bind/asiAsm.pdb DESTINATION ${SDK_INSTALL_SUBDIR}bind CONFIGURATIONS Debug)
endif()

install (FILES ${H_FILES}              DESTINATION ${SDK_INSTALL_SUBDIR}include)
install (FILES ${entity_H_FILES}       DESTINATION ${SDK_INSTALL_SUBDIR}include)
install (FILES ${xde_H_FILES}          DESTINATION ${SDK_INSTALL_SUBDIR}include)
install (FILES ${interop_H_FILES}      DESTINATION ${SDK_INSTALL_SUBDIR}include)
install (FILES ${interop_gltf_H_FILES} DESTINATION ${SDK_INSTALL_SUBDIR}include)
