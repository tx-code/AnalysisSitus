source $env(ASI_TEST_SCRIPTS)/editing/canrec/__begin

# Set working variables.
set datafile public/cad/situ/cr/linear-extrusion_02.brep

# Reference numbers.
set ref_nbSurfBezier     0
set ref_nbSurfSpl        6
set ref_nbSurfConical    0
set ref_nbSurfCyl        8
set ref_nbSurfOffset     0
set ref_nbSurfSph        4
set ref_nbSurfLinExtr    6
set ref_nbSurfOfRevol    0
set ref_nbSurfToroidal   2
set ref_nbSurfPlane      3
set ref_nbCurveBezier    0
set ref_nbCurveSpline    22
set ref_nbCurveCircle    24
set ref_nbCurveEllipse   0
set ref_nbCurveHyperbola 0
set ref_nbCurveLine      20
set ref_nbCurveOffset    0
set ref_nbCurveParabola  0

__convert-canonical
